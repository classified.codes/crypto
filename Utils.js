export function stringToArrayBuffer(string) {
    var encoder = new TextEncoder('utf-8');
    return encoder.encode(string);
}

export function arrayBufferToString(buffer) {
    var decoder = new TextDecoder();
    return decoder.decode(buffer);
}

export function arrayBufferToHexString(arrayBuffer) {
    var byteArray = new Uint8Array(arrayBuffer);
    var hexString = "";
    var nextHexByte;

    for (var i=0; i<byteArray.byteLength; i++) {
        nextHexByte = byteArray[i].toString(16);
        if (nextHexByte.length < 2) {
            nextHexByte = "0" + nextHexByte;
        }
        hexString += nextHexByte;
    }
    return hexString;
}

export function buf2hex(buffer) {
	return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

export function arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array( buffer );
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
    }
    return window.btoa(binary);
}

export function base64ToArrayBuffer(base64) {
    var binary_string =  window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

export function concatBuffers(a, b) {
    let c = new Uint8Array(a.byteLength + b.byteLength);
    c.set(new Uint8Array(a), 0);
    c.set(new Uint8Array(b), a.length);
    return c;
}

export function copyToClipboard(text) {
    let textArea = document.createElement('textarea');
    textArea.value = text;
    window.document.body.appendChild(textArea);
    textArea.select();
    try {
        window.document.execCommand('copy');
    } catch (err) {
        console.log('Failed to copy to clipboard');
    }
    window.document.body.removeChild(textArea);
}

export function post(url, payload = {}) {
    return new Promise((resolve, reject) => {
        let request = new XMLHttpRequest();
        request.addEventListener('load', () => {
            if (request.status === 200) {
                try {
                    let response = JSON.parse(request.responseText);
                    return resolve(response);
                } catch (e) {
                    console.log(e);
                    return reject('Could not parse response as JSON');
                }
            } else {
                try {
                    let response = JSON.parse(request.responseText);
                    return reject(response);
                } catch (e) {
                    return reject(request.responseText);
                }
            }
        });
        request.open('POST', url);
        request.setRequestHeader('Content-Type', 'application/json');
        request.send(JSON.stringify(payload));
    });
}
