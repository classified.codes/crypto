import * as Utils from './Utils.js';

export function pbkdf2(password) {
    return window.crypto.subtle.importKey(
        "raw",
        Utils.stringToArrayBuffer(password),
        {"name": "PBKDF2"},
        false,
        ["deriveKey", "deriveBits"]
    );
}

export function generateRSAKeys() {
    return window.crypto.subtle.generateKey({
        name: 'RSASSA-PKCS1-v1_5',
        modulusLength: 2048,
        publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
        hash: {name: 'SHA-256'}
    }, true, ['sign', 'verify']);
}

export function deriveAESKey(pbkdf2Key, salt) {
    return window.crypto.subtle.deriveKey(
        {
            name: "PBKDF2",
            salt: Utils.stringToArrayBuffer(salt),
            iterations: 1000,
            hash: {name: "SHA-256"}
        },
        pbkdf2Key,
        {name: "AES-CBC", length: 256},
        true,
        ["encrypt", "decrypt"]
    );
}

export function aesEncrypt(key, data, useIV = true) {
    let iv = new Uint8Array(16);
    if (useIV) {
        iv = window.crypto.getRandomValues(new Uint8Array(16));
    }
    return window.crypto.subtle.encrypt({
        name: "AES-CBC",
        iv: iv
    }, key, data)
    .then(result => {
        return useIV ? Utils.concatBuffers(iv, result) : result;
    });
}

export function aesDecrypt(key, data, useIV = true) {
    let iv = new Uint8Array(16);
    if (useIV) {
        iv = new Uint8Array(data.slice(0, 16));
        data = data.slice(16);
    }
    return window.crypto.subtle.decrypt({
        name: "AES-CBC",
        iv: iv
    }, key, data)
}

export function importRSAKey(key) {
    return window.crypto.subtle.importKey(
        'jwk',
        key,
        {
            name: 'RSASSA-PKCS1-v1_5',
            hash: {name: 'SHA-256'}
        },
        false,
        ['sign']
    );
}

export function importAESKey(key) {
    return window.crypto.subtle.importKey(
        'jwk',
        key,
        {
            name: 'AES-CBC',
        },
        false,
        ['encrypt', 'decrypt']
    );
}

export function rsaSign(key, data) {
    return window.crypto.subtle.sign({
        name: "RSASSA-PKCS1-v1_5"
    }, key, data);
}
