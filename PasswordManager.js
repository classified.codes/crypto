import * as Crypto from './Crypto.js';
import * as Utils from './Utils.js';

let baseURL = '';

export function setBaseUrl(url) {
    baseURL = url;
}

export function register(email, aesKey) {
    let request = {};
    let account = {};

    return Utils.post(baseURL + '/api/account-token')
    .then(response => {
        request.accountToken = response.token;
        return Crypto.generateRSAKeys()
    })
    .then(RSAKeys => {
        return Promise.all([
            Crypto.rsaSign(RSAKeys.privateKey, Utils.stringToArrayBuffer(request.accountToken)),
            crypto.subtle.exportKey("jwk", RSAKeys.publicKey),
            crypto.subtle.exportKey("jwk", RSAKeys.privateKey)
        ]);
    })
    .then(results => {
        request.signature = Utils.buf2hex(results[0]);
        request.publicKey = results[1];
        account.privateKey = results[2];
        account.accountToken = request.accountToken;

        return crypto.subtle.digest('SHA-256', Utils.stringToArrayBuffer(email));
    })
    .then(username => {
        request.username = Utils.buf2hex(username);
        let data = Utils.stringToArrayBuffer(JSON.stringify(account));
        return Crypto.aesEncrypt(aesKey, data, false);
    })
    .then(ct => {
        request.account = Utils.arrayBufferToBase64(ct);
        return Utils.post(baseURL + '/api/register', request);
    });
}

export function deriveAESKey(password, email) {
    return Crypto.pbkdf2(password)
    .then(hashedPassword => {
        return Crypto.deriveAESKey(hashedPassword, email)
    });
}

export function save(identity, blob, AESKey) {
    const blobData = Utils.stringToArrayBuffer(JSON.stringify(blob));
    return Crypto.aesEncrypt(AESKey, blobData)
    .then(ct => {
        const blobText = Utils.arrayBufferToBase64(ct);
        return Utils.post(baseURL + '/api/save-blob', {identity: identity, blob: blobText});
    })
    .then(response => {
        return blob;
    });
}

export function fetch(identity, AESKey) {
    return Utils.post(baseURL + '/api/fetch-blob', {identity: identity})
    .then(response => {
        const ct = Utils.base64ToArrayBuffer(response.blob);
        return Crypto.aesDecrypt(AESKey, ct);
    })
    .then(pt => {
        const jsonString = Utils.arrayBufferToString(pt);
        return JSON.parse(jsonString);
    });
}

export function identityCheck(email, aesKey) {
    let identity = {};
    return crypto.subtle.digest('SHA-256', Utils.stringToArrayBuffer(email))
    .then(username => {
        identity.username = Utils.buf2hex(username);
        return Utils.post(baseURL + '/api/identity-test', {username: Utils.buf2hex(username)})
    })
    .then(response => {
        identity.identityTestToken = response.identityTestToken;
        return decryptAccount(response.account, aesKey);
    })
    .then(account => {
        identity.accountToken = account.accountToken;
        return Crypto.importRSAKey(account.privateKey);
    })
    .then(privateKey => {
        return Crypto.rsaSign(privateKey, Utils.stringToArrayBuffer(identity.identityTestToken));
    })
    .then(signatureBuffer => {
        identity.signature = Utils.buf2hex(signatureBuffer);
        return identity;
    });
}

function decryptAccount(accountString, aesKey) {
    let accountDataBuffer = Utils.base64ToArrayBuffer(accountString);
    return Crypto.aesDecrypt(aesKey, accountDataBuffer, false)
    .then(decryptedAccountBuffer => {
        return JSON.parse(Utils.arrayBufferToString(decryptedAccountBuffer));
    });
}
